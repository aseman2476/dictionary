# set
import os

my_set = set()
directory = set()
addresses = '/home/farnaz/GPSRCmdGen/addresses.txt'
address_file = open(addresses, 'r')
address_file = [line.rstrip('\n') for line in address_file]

for address in address_file:

    file = open(address, 'r')

    for line in file:
        if '#' in line:
            continue
        if len(line) > 0:
            my_list = line.split()
            for item in my_list:
                my_set.add(item)

    new_set = set()
    for item in my_set:
        edited_item = ''.join([i for i in item if i.isalpha()])
        if not edited_item == '':
            new_set.add(edited_item.lower())
        dictionary = ','.join(new_set)
print(dictionary)
print(len(dictionary))

f = open("dictionary.txt", "w")
f.write(dictionary)
f.close()
